<?php

// Path to the front controller (this file)
define('FCPATH', __DIR__.DIRECTORY_SEPARATOR);

$pageURL = 'http';
if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
	|| (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')) {
	$pageURL.= 's';
}

$pageURL.= '://';
if (strpos($_SERVER['HTTP_HOST'], ':') !== FALSE) {
	$url = explode(':', $_SERVER['HTTP_HOST']);
	$url[0] ? $pageURL.= $_SERVER['HTTP_HOST'] : $pageURL.= $url[0];
} else {
	$pageURL.= $_SERVER['HTTP_HOST'];
}

$pageURL.= $_SERVER['REQUEST_URI'] ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];

define('FC_NOW_URL', $pageURL);

// Location of the Paths config file.
// This is the first of two lines that might need to be changed, depending on your folder structure.
$pathsPath = FCPATH . 'dayrui/Core/Config/Paths.php';

/*
 *---------------------------------------------------------------
 * BOOTSTRAP THE APPLICATION
 *---------------------------------------------------------------
 * This process sets up the path constants, loads and registers
 * our autoloader, along with Composer's, loads our constants
 * and fires up an environment-specific bootstrapping.
 */

// Ensure the current directory is pointing to the front controller's directory
chdir(__DIR__);

// Load our paths config file
require $pathsPath;
$paths = new Config\Paths();

// Location of the framework bootstrap file.
// This is the second of two lines that might need to be changed, depending on your folder structure.
$app = require FCPATH . 'dayrui/System/bootstrap.php';

/*
 *---------------------------------------------------------------
 * LAUNCH THE APPLICATION
 *---------------------------------------------------------------
 * Now that everything is setup, it's time to actually fire
 * up the engines and make this app do it's thang.
 */
$app->run();
